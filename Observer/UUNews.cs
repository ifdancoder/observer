﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer {
    internal class UUNews {
        List<Observer> observers;
        public UUNews() {
            observers = new List<Observer>();
        }
        public void AddObserver(Observer observer) {
            observers.Add(observer);
        }
        public void RemoveObserver(Observer observer) {
            observers.Remove(observer);
        }
        public void Update(string msg, DateTime msg_date, int msg_type) {
            foreach (Observer obs in observers) {
                obs.Update(msg, msg_date, msg_type);
            }
        }
    }
}
