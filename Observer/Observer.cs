﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer {
    internal interface Observer {
        public void Update(string msg, DateTime msg_date, int msg_type);
    }
}
