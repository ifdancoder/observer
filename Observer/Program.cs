﻿using Observer;
using System.Runtime.ConstrainedExecution;

UUNews station = new UUNews();
InformPolis infpol = new InformPolis();
Agregator agr = new Agregator();
ArigUs arigus = new ArigUs();
station.AddObserver(infpol);
station.AddObserver(arigus);
station.AddObserver(agr);
NewsSource src = new NewsSource(station);
src.Start();
agr.PrintTop5();