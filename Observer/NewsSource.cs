﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer {
    internal class NewsSource {
        string file_name;
        UUNews station;
        public NewsSource(UUNews st) {
            station = st;
            file_name = "news.txt";
        }
        public void Start() {
            StreamReader reader = new StreamReader(file_name);
            string line = reader.ReadLine();
            while (line != null) {
                string[] splitted_row = line.Split(';');
                DateTime dDate;
                if (DateTime.TryParse(splitted_row[1], out dDate))
                {
                    station.Update(splitted_row[0], dDate, Int32.Parse(splitted_row[2]));
                }
                else
                {
                    Console.WriteLine("Invalid date");
                }
                line = reader.ReadLine();
            }
            reader.Close();
        }
    }
}
