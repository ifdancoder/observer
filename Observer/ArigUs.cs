﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer {
    internal class ArigUs : Observer {
        public void Update(string msg, DateTime msg_date, int msg_type) {
            if (msg_type == 3) {
                Console.Write($"[{msg_date.ToShortDateString()}] АРИГУС получил информацию: ");
                Console.WriteLine($"\n\t{msg}");
            }
        }
    }
}
