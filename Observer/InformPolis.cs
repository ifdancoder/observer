﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    internal class InformPolis : Observer
    {
        WeatherInterpreter _w = new WeatherInterpreter();
        public void Update(string msg, DateTime msg_date, int msg_type)
        {
            switch (msg_type)
            {
                case 1:
                    Console.Write($"[{msg_date.ToShortDateString()}] ИНФОРМ ПОЛИС сообщает об изменении погоды: ");
                    msg = _w.PresentWeather(msg);
                    break;
                case 2:
                    Console.Write($"[{msg_date.ToShortDateString()}] ИНФОРМ ПОЛИС сообщает об изменении курса: ");
                    break;
                case 3:
                    Console.Write($"[{msg_date.ToShortDateString()}] ИНФОРМ ПОЛИС сообщает: ");
                    break;
            }
            Console.WriteLine($"\n\t{msg}");
        }
    }
}
