﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer {
    internal class WeatherInterpreter {
        public string PresentWeather(string w) {
            string[] a = w.Split(' ');
            string res = "Температура: " + a[0] + "\n\t";
            switch (a[1]) {
                case "1":
                    res += "Солнечно";
                    break;
                case "2":
                    res += "Дождь";
                    break;
                case "3":
                    res += "Снег";
                    break;
            }
            res += "\n\tСкорость ветра: " + a[2];
            return res;
        }
    }
}
