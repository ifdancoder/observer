﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    internal class Agregator : Observer
    {
        List<double> coefficients = new List<double>() { 0.83, 0.57, 0.95 };
        List<(double, string, DateTime)> top5 = new List<(double, string, DateTime)>();

        public void Update(string msg, DateTime msg_date, int msg_type)
        {
            double new_coefficient = msg.Length * coefficients[msg_type - 1] / 100;
            bool bIsInserted = false;
            for (int i = 0; i < top5.Count; i++)
            {
                if (new_coefficient > top5[i].Item1)
                {
                    if (new_coefficient == top5[i].Item1 && msg_date >= top5[i].Item3)
                    {
                        if (top5.Count > i + 2)
                        {
                            top5.Insert(i + 1, (new_coefficient, msg, msg_date));
                        }
                        else
                        {
                            top5.Add((new_coefficient, msg, msg_date));
                        }
                    }
                    else
                    {
                        top5.Insert(i, (new_coefficient, msg, msg_date));
                    }
                    bIsInserted = true;
                    break;
                }
            }
            if (!bIsInserted && top5.Count < 5)
            {
                top5.Add((new_coefficient, msg, msg_date));
            }
            if (top5.Count > 5)
            {
                top5.RemoveAt(5);
            }
        }
        public void PrintTop5()
        {
            Console.WriteLine($"[{DateTime.Now.ToShortDateString()}] АГРЕГАТОР НОВОСТЕЙ показывает ТОП-5 новостей:");
            for(int i = 0; i < top5.Count; i++)
            {
                Console.WriteLine($"\t{i + 1}) [{top5[i].Item3.ToShortDateString()}] Коэффициент: {Math.Round(top5[i].Item1, 3)}. Сообщение: {top5[i].Item2}");
            }
        }
    }
};